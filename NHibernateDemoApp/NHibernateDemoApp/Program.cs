﻿using NHibernate.Cfg;
using NHibernate.Dialect;
using NHibernate.Driver;

using System;
using System.Linq;
using System.Reflection;
using NHibernate.Cache;
using NHibernate.Dialect.Function;

namespace NHibernateDemoApp
{
    class Program
    {
        static void Main(string[] args)
        {
            App_Start.NHibernateProfilerBootstrapper.PreStart();

            var cfg = new Configuration();
            //cfg.Configure(@"C:\MyStudy\NHibernate_Git\NHibernateDemoApp\NHibernateDemoApp\hibernate.cfg.xml");
            cfg.DataBaseIntegration(x =>
            {               
                x.ConnectionString =
                    @"Data Source=USSCNYUANLI01\MSSQLSERVER2016;Database=NHibernateDemoDB;Integrated Security=False;User ID=sa;Password=1qaz@WSX; Connect Timeout=15;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False";
                x.Driver<SqlClientDriver>();
                x.Dialect<MsSql2012Dialect>();
                x.LogSqlInConsole = true;
                x.BatchSize = 10;
            });

            cfg.AddAssembly(Assembly.GetExecutingAssembly());

            var sefact = cfg.BuildSessionFactory();

            using (var session = sefact.OpenSession())
            {
                using (var tx = session.BeginTransaction())
                {

                    var student1 = new Student
                    {
                        ID = 1,
                        FirstName = "Allan",
                        LastName = "Bommer",
                        AcademicStanding = StudentAcademicStanding.Poor,

                        Address = new Location
                        {
                            Street = "123 Street",
                            City = "Lahore",
                            Province = "Punjab",
                            Country = "Pakistan"
                        }
                    };
                    session.Save(student1);
                    tx.Commit();
                    var students = session.Query<Student>().ToList<Student>();
                    Console.WriteLine("\nFetch the complete list again\n");

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2} \t{3} \t{4} \t{5} \t{6} \t{7}",
                            student.ID,
                            student.FirstName,
                            student.LastName,
                            student.AcademicStanding,
                            student.Address.Street,
                            student.Address.City,
                            student.Address.Province,
                            student.Address.Country
                        );
                    }
                }

                Console.ReadLine();
            }

            //Read Data from Student Table
            using (var session = sefact.OpenSession())
            {

                using (var tx = session.BeginTransaction())
                {
                    var students = session.CreateCriteria<Student>().List<Student>();

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}",
                            student.ID, student.FirstName, student.LastName);
                    }

                    tx.Commit();
                }

                Console.ReadLine();
            }

            using (var session = sefact.OpenSession())
            {

                using (var tx = session.BeginTransaction())
                {
                    var students = session.CreateCriteria<Student>().List<Student>();

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", student.ID,
                            student.FirstName, student.LastName);
                    }

                    var stdnt = session.Get<Student>(1);
                    Console.WriteLine("Retrieved by ID");
                    Console.WriteLine("{0} \t{1} \t{2}", stdnt.ID,
                        stdnt.FirstName, stdnt.LastName);
                    tx.Commit();
                }

                Console.ReadLine();
            }

            //Update Record
            using (var session = sefact.OpenSession())
            {

                using (var tx = session.BeginTransaction())
                {
                    var students = session.CreateCriteria<Student>().List<Student>();

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", student.ID,
                            student.FirstName, student.LastName);
                    }

                    var stdnt = session.Get<Student>(1);
                    Console.WriteLine("Retrieved by ID");
                    Console.WriteLine("{0} \t{1} \t{2}", stdnt.ID, stdnt.FirstName, stdnt.LastName);

                    Console.WriteLine("Update the last name of ID = {0}", stdnt.ID);
                    stdnt.LastName = "Donald";
                    session.Update(stdnt);
                    Console.WriteLine("\nFetch the complete list again\n");

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", student.ID,
                            student.FirstName, student.LastName);
                    }

                    tx.Commit();
                }

                Console.ReadLine();
            }

            //Delete Record
            using (var session = sefact.OpenSession())
            {

                using (var tx = session.BeginTransaction())
                {
                    var students = session.CreateCriteria<Student>().List<Student>();

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", student.ID,
                            student.FirstName, student.LastName);
                    }

                    var stdnt = session.Get<Student>(1);
                    Console.WriteLine("Retrieved by ID");
                    Console.WriteLine("{0} \t{1} \t{2}", stdnt.ID, stdnt.FirstName, stdnt.LastName);

                    Console.WriteLine("Delete the record which has ID = {0}", stdnt.ID);
                    session.Delete(stdnt);
                    Console.WriteLine("\nFetch the complete list again\n");

                    foreach (var student in students)
                    {
                        Console.WriteLine("{0} \t{1} \t{2}", student.ID, student.FirstName,
                            student.LastName);
                    }

                    tx.Commit();
                }

                Console.ReadLine();
            }
        }
    }
}


